import Ember from 'ember';

export default Ember.Component.extend({
    insertMap: function() {
      var container = this.$('.restmap-canvas')[0];
		  var options = {
            center: new window.google.maps.LatLng(
                this.get('latitude'),
                this.get('longitude')
            ),
            zoom: 15
        };
        var map = new window.google.maps.Map(container, options);

        var myLatlng = new google.maps.LatLng(
          this.get('latitude'),
          this.get('longitude')
        );

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title:"Here!"
        });

    }.on('didInsertElement')
});
