import Ember from 'ember';

export default Ember.Route.extend({

  setupController: function (controller, model) {
    this.set('content', model);
    navigator.geolocation.watchPosition(function(res) {
      controller.geoLocation(res);
    });
  }

});
