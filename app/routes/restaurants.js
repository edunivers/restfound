import Ember from 'ember';

export default Ember.Route.extend({

	model: function() {

		var mylat = this.controllerFor('locate').get('mylatitude');
		var mylon = this.controllerFor('locate').get('mylongitude');

		return Ember.$.getJSON('http://overpass-api.de/api/interpreter?data=[out:json];node["amenity"="restaurant"](around:500,'+mylat+','+mylon+');out;').then(
					function(data){
					return data.elements.map(function(restaurant){
						restaurant.type = restaurant.type;
						restaurant.latitude = restaurant.lat;
						restaurant.longitude = restaurant.lon;
						restaurant.city = restaurant.tags["addr:city"];
						restaurant.housenumber = restaurant.tags["addr:housenumber"];
						restaurant.street = restaurant.tags["addr:street"];
						restaurant.phone = restaurant.tags.phone;
						restaurant.website = restaurant.tags.website;
						restaurant.name = restaurant.tags.name;

						return restaurant;
					});
		});
	}
});
