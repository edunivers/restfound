import Ember from 'ember';

export default Ember.Route.extend({
  model: function(params){
    return Ember.$.getJSON('http://overpass-api.de/api/interpreter?data=[out:json];node('+params.restaurant_id+');out;').then(
      function(data){
        return data.elements.map(function(restaurant){
          console.log(restaurant);

          restaurant.type = restaurant.type;
          restaurant.latitude = restaurant.lat;
          restaurant.longitude = restaurant.lon;
          restaurant.city = restaurant.tags["addr:city"];
          restaurant.housenumber = restaurant.tags["addr:housenumber"];
          restaurant.street = restaurant.tags["addr:street"];
          restaurant.phone = restaurant.tags.phone;
          restaurant.website = restaurant.tags.website;
          restaurant.name = restaurant.tags.name;

          return restaurant;
        });
      });
  },

  actions: {
    favourite: function() {
      this.controller.set('favourite', true);
    },

    unfavourite: function() {
      this.controller.set('favourite', false);
    }
  }

});
