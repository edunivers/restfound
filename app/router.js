import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource("locate");
  this.route('about');
  this.resource('restaurants');
  this.resource('restaurant', {path: '/restaurants/:restaurant_id'});
});

export default Router;
