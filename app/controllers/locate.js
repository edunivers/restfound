import Ember from 'ember';

export default Ember.Controller.extend({
  //mylatitude: 0,
  //mylongitude: 0,

  geoLocation: function (position) {
        this.set('mylatitude', position.coords.latitude);
        this.set('mylongitude', position.coords.longitude);
  },

  markers: [{}],

  addMarker: function () {
        this.get('mymarkers').addObject({
          title: 'My Location',
          lat: this.get('mylatitude'),
          lng: this.get('mylongitude'),
          isDraggable: true
        });
  }

});
