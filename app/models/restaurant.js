import DS from 'ember-data';

export default DS.Model.extend({
	type: DS.attr('string'),
	feature: DS.attr('string'),
	city: DS.attr('string'),
	housenumber: DS.attr('string'),
	street: DS.attr('string'),
	phone: DS.attr('string'),
	website: DS.attr('string'),
	latitude: DS.attr('double'),
	longitude: DS.attr('double'),
	name: DS.attr('string'),
	favourite: DS.attr('boolean', {defaultValue: false})
});
